package io.getarrays.server.service.implementation;

import io.getarrays.server.model.Server;
import io.getarrays.server.repository.ServerRepository;
import io.getarrays.server.service.ServerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Random;

import static io.getarrays.server.enumeration.Status.SERVER_UP;
import static io.getarrays.server.enumeration.Status.SERVER_DOWN;
import static java.lang.Boolean.TRUE;

@RequiredArgsConstructor // (*) create the constructor for the serverRepo variable
@Service
@Transactional
@Slf4j // Print in the console what is happening
public class ServerServiceImplementation implements ServerService {
    private final ServerRepository serverRepo;

    /**
     * @param server
     * @return
     */
    @Override
    public Server create(Server server) {
        log.info("Saving new server: {}", server.getName());
        server.setImageUrl(setServerImageUrl());
        return serverRepo.save(server); // (*) save the server in the database
    }

    /**
     * @param ipAddress
     * @return
     */
    @Override
    public Server ping(String ipAddress) throws IOException {
        log.info("Pinging server IP: {}", ipAddress);
        Server server = serverRepo.findByIpAddress(ipAddress);
        InetAddress address = InetAddress.getByName(ipAddress);
        server.setStatus(address.isReachable(10000) ? SERVER_UP : SERVER_DOWN); // If reachable within this timeout then set to UP otherwise DOWN
        serverRepo.save(server);
        return server;
    }

    /**
     * @param limit
     * @return
     */
    @Override
    public Collection<Server> list(int limit) {
        log.info("Fetching all servers");
        return serverRepo.findAll(PageRequest.of(0, limit)).toList(); // (*) 0 --> to start from the beginning
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Server get(Long id) {
        log.info("Fetching server by id: {}", id);
        return serverRepo.findById(id).get();
    }

    /**
     * @param server
     * @return
     */
    @Override
    public Server update(Server server) {
        log.info("Updating server: {}", server.getName());
        return serverRepo.save(server); // (*) here if the id exists it will update the server in the database, otherwise it will create a new one
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Boolean delete(Long id) {
        log.info("Deleting server by ID: {}", id);
        serverRepo.deleteById(id);
        return TRUE; // (*) if everything ok it will return true otherwise it will generate a error before the return
    }

    private String setServerImageUrl() {
        String[] imageNames = {"server1.png", "server2.png", "server3.png"};
        return ServletUriComponentsBuilder.fromCurrentContextPath().path("/images/" + imageNames[new Random().nextInt(3)]).toUriString();
    }
}
