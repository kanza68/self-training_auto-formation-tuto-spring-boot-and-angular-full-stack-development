package io.getarrays.server.repository;

import io.getarrays.server.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;

// (*) JpaRepository<domain , type PK>The domain or class we want to manage and the type of the primary key
// (*) by extends the JpaRepository class give access to several methods that are ready to use
public interface ServerRepository extends JpaRepository<Server, Long> {
    // JPA will interpret the name of the method to create a query
    Server findByIpAddress(String ipAddress);
}
