# Self-training_Auto-formation - Tuto Spring Boot and Angular Full Stack Development - BACK

 - [Version en Français](#item-one)
 - [English version](#item-two)

 <a id="item-one"></a>
 ## Version en Français

### Introduction
Ce projet est basé sur un tutoriel.

Les technologie utilisées sont :
- Maven
- PostgreSQL
- Spring Web
- Spring Data
- Spring Boot
- L'IDE IntelliJ 

### Objectif de ce projet
Ce projet fait partie de mon objectif de maintien et d'amélioration de mes compétence pour cette année 2024.

### Sources
Ce projet se base sur un [tutoriel sur Spring Boot et Angular](https://youtu.be/8ZPsZBcue50?si=jVqdwaS5WkVR89ai) publié par AmigosCode sur Youtube



<a id="item-two"></a>
## English version

### About the project
This project is based on a tutorial

The tools used are :
- Maven
- PostgreSQL
- Spring Web
- Spring Data
- Spring Boot
- IntelliJ IDE

### Goal of this project
This project is part of my objective to maintain and improve my skills for 2024.

### Sources
For this project I use a [Spring Boot and Angular tutorial](https://youtu.be/8ZPsZBcue50?si=jVqdwaS5WkVR89ai) published by AmigosCode on YouTube
